# Barbeque

***
Barbeque is a events organizer application.
The purpose of the application is to help people organize events, meetings in cafes and restaurants.

##### Web page:
[Barbeque](https://barbeque.herokuapp.com) on heroku

### Launching
***
##### Requirements:
* Ruby version _>= 2.4.1_
* Rails version _'~> 5.0.7'_

##### Commands
Clone the repository to your local machine and run following commands:

```
$ bundle
```

```
$ bundle exec rake db:migrate
```

```
$ bundle exec rails s
```

Open a `localhost:3000` page in your browser.

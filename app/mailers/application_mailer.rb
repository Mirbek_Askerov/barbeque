class ApplicationMailer < ActionMailer::Base
  default from: 'test.userbey@gmail.com'
  layout 'mailer'
end

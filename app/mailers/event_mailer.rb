class EventMailer < ApplicationMailer
  def subscription(event, subscription)
    @email = subscription.user_email
    @name = subscription.user_name
    @event = event

    mail to: event.user.email, subject: I18n.t("actionmailer.subscription", title: event.title)
  end

  def comment(event, comment, email)
    @comment = comment
    @event = event

    mail to: email, subject: I18n.t("actionmailer.comment", title: event.title)
  end

  def photo(event, photo, email)
    @event = event
    @photo = photo

    @image_name = photo.photo.filename
    attachments.inline[@image_name] = photo.photo.read

    mail to: email, subject: I18n.t("actionmailer.photo", title: event.title)
  end
end

class Subscription < ApplicationRecord
  belongs_to :event
  belongs_to :user, optional: true

  validates :event, presence: true

  # проверки выполняются только если user не задан (незарегистрированные приглашенные - анонимы)
  validates :user_name, presence: true, unless: 'user.present?'
  validates :user_email, presence: true, format: /\A[a-zA-Z0-9\-_.]+@[a-zA-Z0-9\-_.]+\z/, unless: 'user.present?'

  validates :user, uniqueness: {scope: :event_id}, if: 'user.present?'
  validates :user_email, uniqueness: {scope: :event_id}, unless: 'user.present?'

  validate :event_author?
  validate :email_exist?, unless: 'user.present?'

  def user_name
    if user.present?
      user.name
    else
      super
    end
  end

  def user_email
    if user.present?
      user.email
    else
      super
    end
  end

  private

    def event_author?
      if event.user == user
        errors.add(:empty, I18n.t('errors.messages.owner'))
      end
    end

    def email_exist?
      errors.add(:user_email, I18n.t('errors.messages.taken')) if User.exists?(email: user_email)
    end
end
